###############################################################################.
#' Extract reduced form t-statistic from model 1 type of data
#'
#' \code{get_t_stat} will fit a VAR without constant to the data and return
#' the three t-statistics on the free coefficients in model 1.
#'
#' @param data A (K x T) matrix, containing the data of the K variables.
#' @param p An integer, the number of lags to include in the estimation.
#'
#' @seealso \code{\link{draw_data_DH08_m1}}
#'
get_t_stat <- function(data, p = 1) {

  K <- 4
  alpha <- rep(0.7, 3)
  A_0 <- diag(K)
  A_0[4, 1:3] <- alpha

  COEFS <- matrix(, nrow = 1000, ncol = 7)

  for (i in 1:1000) {

    data <- draw_data_DH08_m1(n = 5E3, alpha = alpha)

    # Amat <- matrix(0, nrow(data), nrow(data))
    # Amat[lower.tri(Amat, diag = T)] <- NA
    # # diag(Amat) <- NA
    # # Amat[4, 1:4] <- NA
    # var_fit  <- vars::VAR(t(data), type = "none", p = p)
    # svar_fit <- vars::SVAR(var_fit,
    #                        estmethod = "direct",
    #                        # start = rep(0.2, 3),
    #                        Amat = Amat,
    #                        lrtest = FALSE,
    #                        hessian = TRUE)

    # equation by equation OLS - only need fourth row

    # create matrix of lags??!!


    fit <- lm(
      data[4, -1] ~ I(-data[1, -1]) + I(-data[2, -1]) + I(-data[3, -1]) - 1 +
        data[1, -(5e3)] + data[2, -(5e3)] + data[3, -(5e3)] + data[4, -(5e3)]
    )

    COEFS[i, ] <- coefficients(fit)
    print(i)
  }

  #13.06 Late afternoon

  # HUGE BIAS! when structural AND RESCALING ERRORS!!
  # will t-stats be informative? can control signal-to-noise
  # without rescaling???
  colMeans(COEFS)
  hist(COEFS[, 1], breaks = 20)
  hist(COEFS[, 6], breaks = 20)






  svar_fit
  solve(A_0)
  svar_fit$Ase
  summary(svar_fit)

  summary(var_fit)$varresult[[4]]$coefficients[1:3, "t value"]
}


####
# ONE OR SEVERAL COEFFS DOES NOT MATTER (AS IT SHOULDNT)
####

###############################################################################.
#' Extract reduced form t-statistic from model 1 type of data
#'
#' \code{get_t_stat} will fit a VAR without constant to the data and return
#' the three t-statistics on the free coefficients in model 1.
#'
#' @param data A (K x T) matrix, containing the data of the K variables.
#' @param p An integer, the number of lags to include in the estimation.
#'
#' @seealso \code{\link{draw_data_DH08_m1}}
#'
get_t_stat <- function(data, p = 1) {



  K <- 4

  A_0 <- diag(K)
  A_0[4, 1:3] <- alpha

  COEFS <- matrix(, nrow = 1000, ncol = 2)
  set.seed(1234)
  alpha <- matrix(runif(3 * 1000, -1, 1), ncol = 3)


  for (i in 1:1000) {
    data <- draw_data_DH08_m1(n = 5E2, alpha = c(alpha[i, ]))

    # Amat <- matrix(0, nrow(data), nrow(data))
    # Amat[lower.tri(Amat, diag = T)] <- NA
    # # diag(Amat) <- NA
    # # Amat[4, 1:4] <- NA
    # var_fit  <- vars::VAR(t(data), type = "none", p = p)
    # svar_fit <- vars::SVAR(var_fit,
    #                        estmethod = "direct",
    #                        # start = rep(0.2, 3),
    #                        Amat = Amat,
    #                        lrtest = FALSE,
    #                        hessian = TRUE)

    # equation by equation OLS - only need fourth row

    # create matrix of lags??!!


    fit <- lm(
      data[4, -1] ~ I(-data[1, -1]) + I(-data[2, -1]) + I(-data[3, -1]) - 1 +
        data[1, -(5e2)] + data[2, -(5e2)] + data[3, -(5e2)] + data[4, -(5e2)]
    )

    COEFS[i, ] <- c(alpha[i, 1], summary(fit)$coefficients[1, "t value"])
    print(i)
  }

  #13.06 Late afternoon

  # HUGE BIAS! when structural AND RESCALING ERRORS!!
  # will t-stats be informative? can control signal-to-noise
  # without rescaling???
  plot(COEFS[, 1], COEFS[, 2])




  svar_fit
  solve(A_0)
  svar_fit$Ase
  summary(svar_fit)

  summary(var_fit)$varresult[[4]]$coefficients[1:3, "t value"]
}



