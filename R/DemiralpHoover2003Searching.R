###############################################################################.
# You can learn more about package authoring with RStudio at:
#
#   http://r-pkgs.had.co.nz/
#
# Some useful keyboard shortcuts for package authoring:
#
#   Build and Reload Package:  'Ctrl + Shift + B'
#   Check Package:             'Ctrl + Shift + E'
#   Test Package:              'Ctrl + Shift + T'
###############################################################################.
#' Model 1 of Demiralp and Hoover (2008)
#'
#' Generate data according to what Demiralp and Hoover (2008) describe for the
#' first model in their simulation exercise.
#'
#' @param n An integer, defining the number of observations.
#' @param alpha A vector, defining the three free parameters.
#' @param nburnin An integer, the number of burn-in observations.
#'
#' @export
draw_data_DH08_m1 <- function(n = 500, alpha = runif(3, 0, 1), nburnin = 1500) {
  K <- 4
  n_all <- n + nburnin
  # create coefficient matrix of lag one
  A_1 <- matrix(0.05, ncol = K, nrow = K)
  diag(A_1) <- 0.25
  # create contemporaneous coefficient matrix
  A_0 <- diag(K)
  A_0[K, 1:3] <- alpha
  # draw error terms as NIID(0, 1)
  E <- matrix(rnorm(K * n_all, 0, 1), ncol = n_all, nrow = K)
  # reduced form
  A <- solve(A_0) %*% A_1
  EE <- solve(A_0) %*% E

  # normalise errors
  # EE <- EE / sqrt(diag(A_0 %*% t(A_0)))

  # start values
  Y_0 <- matrix(0, nrow = K, ncol = 1)

  create_var1_data(A, Y_0, EE)[, (1:n) + nburnin]
}
###############################################################################.
#' Model 1 of Demiralp and Hoover (2003)
#'
#' Generate data according to what Demiralp and Hoover (2003) describe for the
#' first model in their simulation exercise.
#'
#' @param n An integer, defining the number of observations.
#' @param alpha A vector, defining the three free parameters.
#' @param nburnin An integer, the number of burn-in observations.
#'
#' @export
draw_data_DH03_m1 <- function(n = 500, alpha = runif(3, 0, 1), nburnin = 1500) {
  K <- 4
  n_all <- n + nburnin
  # create coefficient matrix of lag one
  A_1 <- matrix(0.054, ncol = K, nrow = K);  diag(A_1) <- 0.0403
  A_2 <- matrix(0.00292, ncol = K, nrow = K);  diag(A_2) <- 0.162
  A_3 <- matrix(0.000517, ncol = K, nrow = K);  diag(A_3) <- 0.0654
  A_4 <- matrix(0.00000850, ncol = K, nrow = K);  diag(A_4) <- 0.0264
  A   <- cbind(A_1, A_2, A_3, A_4)
  # create contemporaneous coefficient matrix
  A_0 <- diag(K)
  A_0[K, 1:3] <- alpha
  # draw error terms as NIID(0, 1)
  U <- matrix(rnorm(K * n_all, 0, 1), ncol = n_all, nrow = K)
  # reduced form
  B <- solve(A_0) %*% A
  UU <- solve(A_0) %*% U

  # normalise errors
  # UU <- UU / sqrt(diag(A_0 %*% t(A_0)))

  # var(t(UU / sqrt(diag(A_0 %*% t(A_0)))))
  # check that recycling works correctly!


  # start values
  Z_0 <- matrix(0, nrow = K, ncol = 4)

  create_varp_data(B, Z_0, UU)[, (1:n) + nburnin]
}
###############################################################################.
#' Create data using a VAR(1)
#'
#' Given some starting values, coefficients and a sequence of error vectors,
#' \code{create_var1_data} will compute a sequence of observables according to a
#' vector autoregressive model of order one.
#'
#' @param A   A (K x K) matrix, providing the coeffcients for lag one.
#' @param Y_0 A (K x 1) vector or column matrix, will be used as starting
#'   values.
#' @param EE  A (K x T) matrix, providing the sequence of error vectors.
#' @return A (K x T) matrix of observables. The first column will equal \code{A
#'   \%*\% Y_0 + EE[, 1]}. The final observation of the K variables will be in
#'   column T.
#' @section Note: For a faster implementation, see
#'   \href{http://gallery.rcpp.org/articles/simulating-vector-autoregressive-process/}{this
#'    solution} by Dirk Eddelbuettel.
#' @export
create_var1_data <- function(A, Y_0, EE) {
  YY <- matrix(, ncol = ncol(EE), nrow = nrow(EE))
  rownames(YY) <- paste0("y", seq_len(nrow(EE)))
  YY[, 1] <- A %*% Y_0 + EE[, 1]
  for (t in seq_len(ncol(EE) - 1)) {
      YY[, t + 1] = A %*% YY[, t] + EE[, t + 1]
    }
  return(YY)
}
###############################################################################.
#' Create data using a VAR(p)
#'
#' Given some starting values, coefficients and a sequence of error vectors,
#' \code{create_varp_data} will compute a sequence of observables according to a
#' vector autoregressive model of order p.
#'
#' @param B   A (K x Kp) matrix, providing the coeffcients for lag 1 - p.
#' @param Z_0 A (K x p)  matrix, will be used as starting values.
#' @param UU  A (K x T) matrix, providing the sequence of error vectors.
#' @return A (K x T) matrix of observables. The first columns will be equal to
#'   \code{Z_0}. The final observation of the K variables will be in column T.
#' @section Note: For a faster implementation, see
#'   \href{http://gallery.rcpp.org/articles/simulating-vector-autoregressive-process/}{this
#'    solution} by Dirk Eddelbuettel.
#' @export
create_varp_data <- function(B, Z_0, UU) {
  # no intercept
  K <- nrow(B)
  p <- ncol(B) / K
  Tt <- ncol(UU)

  YY <- matrix(, ncol = Tt, nrow = K)
  rownames(YY) <- paste0("y", seq_len(K))
  YY[, 1:p] <- Z_0
  for (t in p:(Tt - 1)) {
    YY[, t + 1] = B %*% as.vector(YY[, t:(t-p+1)]) + UU[, t + 1]
  }
  return(YY)
}
###############################################################################.
#' Extract t-statistic of contemporaneous coefficient from model 1 type of data
#'
#' \code{get_t_stat} will fit a VAR(p) without constant to the data and return
#' the t-statistic belonging to the contemporaneous coefficient of the first
#' variable in equation four of model 1.
#'
#' The fourth equation of model 1 is estimated using straightforward OLS. On the
#' right-hand side there are lags of variables one to four as well
#' contemporaneous variables one, two, three. Since the system is recursive,
#' estimation with OLS gives unbiased and consistent results. The other two
#' contemporaneous coefficients are not required as they are drawn independently
#' in \code{\link{analyse_ex_ante}} and will not affect overall results.
#'
#' @param data A (K x T) matrix, containing the data of the K variables.
#' @param p An integer, the number of lags to include in the estimation.
#'
#' @return An integer, the t-statistic.
#'
#' @seealso \code{\link{draw_data_DH08_m1}}
#'
get_t_stat <- function(data, p = 1) {
  # create Z matrix (which includes lags) and transpose it for lm()
  tZ <- t(Y2Z(Y = data, p = p))
  # equation by equation OLS - only need fourth row - no constant
  fit <- lm(tZ[, 4] ~ tZ[, -4] - 1)
  # return t-value of first structural coefficient in fourth row (a_41)
  summary(fit)$coefficients[1, "t value"]
}
###############################################################################.
#'Find mapping between coefficients and t-statistics
#'
#'Run ex ante analysis of mapping between coefficients and t-statistics.
#'
#'For the simulation exercise, a mapping between coefficient values and
#'t-statistics is required. This mapping serves as an indication of the
#'approximat signal-to-noise ratio of a given dgp. This function provides this
#'mapping by repeatedly drawing coefficient values, creating data, fitting a
#'model and evaluation the t-statistics. Finally, a regression is run to obtain
#'a mapping between initial coefficient value and t-stat.
#'
#'@param seed An integer, the seed for the random number generator.
#'@param reps An integer, the number of times to draw coefficient values.
#'@param data_fn A function, creates the artificial data. It needs to be able to
#'  accept an argument called \code{alpha} and return a (K x T) matrix of data.
#'  \code{alpha} is a vector of three elements and will be used to fix the three
#'  free coefficients in the model that \code{data_fn} is meant to create data
#'  for. Currently the last two elements of \code{alpha} are set to zero and the
#'  first is used to pass on a single cofficient value to the function. See
#'  \code{\link{draw_data_m1}} for an example to pass to \code{data_fn}.
#'@param limits A vector, the limits of the uniform distribution from which
#'  coeffcient values are sampled.
#'@param const An integer, set to 0 or -1 to exclude a constant from the
#'  regression.
#'@return An object of class \code{lm}. The result of running a linear
#'  regression between t-statistics and coefficient values.
#'@section Warning: The relation between t-stats and coefficients does not seem
#'  to be linear, and a linear regression does not seem appropriate. For a
#'  better indication of the signal-to-noise ratio other methods, like
#'  non-linear least squares, might be more appropriate. Nonetheless, this
#'  function serves to replicate (hopefully) the results of Demiralp and Hoover
#'  (2003).
#'@references \insertRef{DemiralpHoover2003Searching}{causalSVAR}
#'@export
analyse_ex_ante <- function(seed = 2^14 - 1, reps = 1E4,
                            data_fn = draw_data_DH03_m1, limits = c(-3, 3),
                            const = 1) {
  set.seed(seed)

  betas <- replicate(reps, runif(3, limits[1], limits[2]), simpl = F)
  data <- lapply(betas, function(x) data_fn(alpha = c(x[1], 0, 0)))

  tstats <- lapply(data, get_t_stat)
  tstats <- do.call(rbind, tstats)

  ts <- tstats[, 1] # previously get_t_stat returned more than one t-stat
  bt <- do.call(rbind, betas)[, 1]

  lm(as.formula(paste0("ts ~ bt +", const)))
}
###############################################################################.
#' Execute mapping between cofficients and t-statistic
#'
#' In order to evaluate the signal-to-noise ratio implied by a given cofficient
#' value, use this function to get an approximate indication of that ratio by
#' relying on the expected t-statistic of that cofficient.
#'
#'@param beta An vector, the cofficient values to be mapped.
#'@param coeffs A vector, length two, the parameters of the mapping function.
#'@return A named vector with the same length as beta, containing the mapped
#'  t-stat for each coefficient value.
#'@export
coeff2tstat <- function(betas, coeffs) {
  out <- c(coeffs %*% rbind(1, betas))
  # if no name, give one
  if (is.null(names(out))) names(out) <- paste0("tstat", seq_along(out))
  out
}
###############################################################################.
#'Remove direction of edges in adjacency matrix
#'
#'All edges in the resulting adjacency matrix will be bi-directional.
#'
#'@param adj_mat An adjacency matrix
#'
#'@return A symmetric matrix. All ones in the lower triangel will have been
#' copied to the upper triangel and vice versa.
delete_direction <- function(adj_mat) {
  adj_mat[t(adj_mat) == 1] <- 1
  adj_mat
}
###############################################################################.
#'Find number of omitted edges
#'
#'Given two adjacency matrices, one true, one estimated, find the discrepancies
#'between them in terms of omitted edges in the underlying estimated graph.
#'
#'@param ge An object of class \code{graphNEL}, the estimated graph.
#'@param gt An object of class \code{graphNEL}, the true graph.
#'
#'@return An integer. The number of links which have been omitted in the
#'  estimated graph.
#'@export
# TODO: wrap middle part (ugraph, wgtMatrix, order) in function and test
eval_omitted <- function(ge, gt) {
  check_graphs_compatible(ge, gt)

  geu <- graph::ugraph(ge)
  gtu <- graph::ugraph(gt)

  meu <- pcalg::wgtMatrix(geu)
  mtu <- pcalg::wgtMatrix(gtu)

  # keep order
  order_mtu <- colnames(mtu)
  meu <- meu[order_mtu, order_mtu]

  # check in those places where edges are
  sum(!meu[mtu == 1]) / 2
}
###############################################################################.
#'Find number of misattributed edges
#'
#'Given two graphs, one true, one estimated, find the discrepancies
#'between them in terms of falsely attributed edges in the estimated graph.
#'
#'@inheritParams eval_omitted
#'@return An integer. The number of links which have been assigned in
#'  the estimated graph, even though they do not exist in the true graph.
#'@export
eval_committed <- function(ge, gt) {
  check_graphs_compatible(ge, gt)

  geu <- graph::ugraph(ge)
  gtu <- graph::ugraph(gt)

  meu <- pcalg::wgtMatrix(geu)
  mtu <- pcalg::wgtMatrix(gtu)

  # keep order
  order_mtu <- colnames(mtu)
  meu <- meu[order_mtu, order_mtu]

  # check in those places where no edges are
  sum(meu[!mtu]) / 2
}
###############################################################################.
#'Find number of reversed edges
#'
#'Given two graphs, one true, one estimated, find the discrepancies
#'between them in terms of falsely directed edges in the estimated graph.
#'
#'@inheritParams eval_omitted
#'@return An integer. The number of links in the estimated graph which have been
#'  correctly identified as edges but point in the wrong direction.
#'@export
eval_reversed <- function(ge, gt) {
  check_graphs_compatible(ge, gt)

  me <- pcalg::wgtMatrix(ge)
  mt <- pcalg::wgtMatrix(gt)

  # keep order
  order_mt <- colnames(mt)
  me <- me[order_mt, order_mt]

  # check in those places where true edges from A to B, not vice versa,
  # if estimated edges from B to A, but not vice versa.
  indx <-(mt == 1) * (t(mt) == 0) == 1
  sum(t(me)[indx] == 1 & me[indx] == 0)
}
###############################################################################.
#'Find number of unresolved edges
#'
#'Given two graphs, one true, one estimated, find the discrepancies between them
#'in terms of unresolved or bi-directed edges in the estimated graph.
#'
#'@inheritParams eval_omitted
#'@return An integer. The number of links in the estimated graph which have been
#'  correctly identified as edges but point in both directions. In a directed
#'  acyclical graph (which the true graph is assumed to be), this counts as
#'  unresolved.
#'@export
eval_unresolved <- function(ge, gt) {
  check_graphs_compatible(ge, gt)

  me <- pcalg::wgtMatrix(ge)
  mt <- pcalg::wgtMatrix(gt)

  # keep order
  order_mt <- colnames(mt)
  me <- me[order_mt, order_mt]

  # check in those places where true edges from A to B, not vice versa,
  # if estimated edges from B to A and A to B.
  indx <-(mt == 1) * (t(mt) == 0) == 1
  sum(t(me)[indx] == 1 & me[indx] == 1)
}
###############################################################################.
#'Determine whether estimated graph is correct
#'
#'Given two graphs, one true, one estimated, determine whether they coincide.
#'This includes the presence and absence of edges as well as their orientation.
#'
#'@inheritParams eval_omitted
#'@return Boolean. If both graphs coincide, return TRUE, otherwise FALSE.
#'@export
is_correct <- function(ge, gt) {
  check_graphs_compatible(ge, gt)

  me <- pcalg::wgtMatrix(ge)
  mt <- pcalg::wgtMatrix(gt)

  # keep order
  order_mt <- colnames(mt)
  me <- me[order_mt, order_mt]

  # are both matrices the same (apart from attributes)?
  isTRUE(all.equal(me, mt))
}
###############################################################################.
#'Determine whether estimated graph is skeleton correct
#'
#'Given two graphs, one true, one estimated, determine whether their skeletons
#'coincide. This includes checking the presence and absence of edges but not
#'their orientation.
#'
#'@inheritParams eval_omitted
#'@return Boolean. If both graphs coincide up to their skeleton, return TRUE,
#'  otherwise FALSE.
#'@export
is_skeleton_correct <- function(ge, gt) {
  check_graphs_compatible(ge, gt)

  # make undirected
  geu <- graph::ugraph(ge)
  gtu <- graph::ugraph(gt)

  meu <- pcalg::wgtMatrix(geu)
  mtu <- pcalg::wgtMatrix(gtu)

  # keep order
  order_mtu <- colnames(mtu)
  meu <- meu[order_mtu, order_mtu]

  # are both matrices the same?
  isTRUE(all.equal(meu, mtu))
}
##############################################################################.
#'Check if two graphs can be compared
#'
#'Given two graphs, check whether they are of class \code{graphNEL} and whether
#'they have the same number of nodes.
#'
#'@inheritParams eval_omitted
#'@return NULL. Used for its side-effects. If any of the conditions are
#'  violated, the function will throw an error.
#'@export
check_graphs_compatible <- function(ge, gt) {
  stopifnot(is(ge, "graphNEL"))
  stopifnot(is(gt, "graphNEL"))
  stopifnot(all(ge@nodes %in% gt@nodes) && all(gt@nodes %in% ge@nodes))
  if (!identical(ge@nodes, gt@nodes)) return(FALSE)
  return(TRUE)
}

# TODO Test and Document !
align_node_order <- function(ge, gt) {
# TODO !!
}
###############################################################################.
#'Get true graph of model one of Demiralp and Hoover (2003)
#'
#'Create \code{graphNEL} object that represents model one used for generating
#'data in the simulation study performed by Demiralp and Hoover (2003).
#'
#'@return An object of class \code{graphNEL} with four nodes. There are directed
#'  edges from nodes 1, 2, 3 to node 4.
#'@export
get_tg <- function() {
  eL <- replicate(4, list(list(edges = 4)))
  eL[[4]] <- list()
  names(eL) <-1:4  # same names as node
  graph::graphNEL(nodes = as.character(1:4),
                  edgeL = eL,
                  edgemode = "directed")
}
###############################################################################.
#'Simulate model one of Demiralp and Hoover (2003)
#'
#'\code{simulate_model_1} will simulate model one once, estimate a graph and
#'report the differences to the reference graph (in this case the true graph).
#'
#'A single call to this function will perform one simulation of model "one".
#'Three random coefficients will be drawn, data will be created, the ex-ante
#'t-statistics will be recorded and a graph will be estimated using the
#'\code{\link[pcalg]{pc}} algorithm. The estimated graph will be compared to its
#'reference graph and the differences reported.
#'The distribution of the coefficients depends on the \code{cut_off} points that
#'are fed to \code{simulate_model_1}. The cut-off points serve as limits of two
#'uniform distributions. The mapping between the randomly drawn coefficient
#'values and the ex-ante t-statistics is performed using the previously fitted
#'linear function with parameters \code{coeffs}.
#'
#'@param cut_off A vector with three elements. The limits of two adjacent
#'  uniform distributions which are used to sample the coefficient values. The
#'  first and the second elements are limits of a uniform distribution which
#'  will be sampled in 2/3 of the cases. The second and third entries are the
#'  limits of a uniform distribution which will be sampled in 1/3 of cases.
#'@param coeffs A vector with two elements. The first element is the intercept,
#'  the second the slope from a regression of t-statistics on coefficient
#'  values. The vector will be used to map the randomly drawn coefficients to
#'  their ex-ante t-statistics.
#'@return A data frame with one row and several columns. The columns list the
#'  ex-ante t-statistic of the randomly drawn coefficient as well as the
#'  different evaluations of the estimated graph. The latter consist of
#'  evaluating whether the graphs is correct, skeleton correct, how many edges
#'  have been falsely attribute, falsely omitted, or whether they were
#'  unresolved or reversed. The four latter statistics are expressed as
#'  fractions of possible outcomes. That is, a one would indicate that all
#'  instances, where the respective error could have occurred, have in fact
#'  occurred, while a zero would indicate that none have occurred. As another
#'  example, a 0.5 would indicate that half of those edges which could
#'  potentially be falsely attributed, were in fact falsely attributed.
#'@export
simulate_model_1 <- function(cut_off, coeffs, sign_lvl = 0.1) {
  # where to draw coefficients? repeat for 200 coeff or draw everytime?

  # draw everytime
  alpha <- replicate(3, {
    w <- sample(0:1, 1, prob = c(1/3, 2/3))
    w       * runif(1, cut_off[1], cut_off[2]) +
    (1 - w) * runif(1, cut_off[2], cut_off[3])
  })

  data <- draw_data_DH03_m1(alpha = alpha)

  var_fit <- vars::VAR(t(data), type = "none", p = 1)
  U <- vars:::residuals.varest(var_fit)
  Sigma <- t(U) %*% U / (nrow(U) - ncol(U))

  # check ex-ante t-stats
  tstats <- coeff2tstat(alpha, coeffs)

  pc_fit <- pcalg::pc(suffStat = list(C = cov2cor(Sigma), n = nrow(U)),
                      indepTest = pcalg::gaussCItest,
                      alpha = sign_lvl,
                      p = ncol(U))

  # eval result
  true_graph <- get_tg()
  # true graph must be a DAG ! does not work with "reference" graph | CPDAG
  num_edges <- graph::numEdges(true_graph)
  num_nodes <- length(true_graph@nodes)
  num_no_edges <-  ((num_nodes^2 - num_nodes)/2) - num_edges

  result <- list(
    correct    = is_correct(         pc_fit@graph, true_graph),
    skcorrect  = is_skeleton_correct(pc_fit@graph, true_graph),
    committed  = eval_committed(     pc_fit@graph, true_graph) / num_no_edges,
    omitted    = eval_omitted(       pc_fit@graph, true_graph) / num_edges,
    reversed   = eval_reversed(      pc_fit@graph, true_graph) / num_edges,
    unresolved = eval_unresolved(    pc_fit@graph, true_graph) / num_edges
  )

  # check if results make sense
  with(result, {
    sum_co <- committed  + omitted
    sum_coru <- sum_co + reversed + unresolved

    stopifnot(skcorrect || (skcorrect == correct))
    stopifnot(skcorrect == (sum_co == 0))
    stopifnot(correct   == (sum_coru == 0))
    stopifnot((sum_coru - committed) <= 1)
    stopifnot(committed <= 1)
  })

  # and store together with t-stats
  as.data.frame(c(tstats, result))
}
###############################################################################.
#'Categorise t-statistics
#'
#'Given a single t-stat value, \code{tstat2cat} will report in which category
#'the value falls. The categories are low, middle and high.
#'\code{tstat2cat_list} will expect a vector of t-statistics and return a vector
#'of characters.
#'
#'@param tstat A scalar. The value of the t-statistic which will be categorised
#'  in "low", "middle" or "high".
#'@return A single character. "l" for low, "m" for middle, "h" for high. The
#'  cut-offs are (in the same order) i) below 2, ii) between 2 and 5, iii) above
#'  5.
#'@export
tstat2cat <- function(tstat) {
  if (tstat <= 2) return("l") # low
  if (tstat > 2 && tstat <= 5) return("m") # middle
  if (tstat > 5) return("h") # high
}
###############################################################################.
#'@rdname tstat2cat
#'@export
tstat2cat_list <- Vectorize(tstat2cat)
###############################################################################.
#'Retrieve a particular combination of a character vector.
#'
#'\code{lookup_entry} will look up a particular combination of character strings
#'stored in \code{ref}. The particular combinations to be returned are provided
#'by \code{lookup_tbl}. This is useful when mapping between a permutation with
#'repetition (n-tuples) and a combination with repetition.
#'\code{lookup_entry_list} will expect a list of character vectors and return a
#'list.
#'
#'@param ref A vector, the elements of which are to be ordered.
#'@param lookup_tbl A matrix. Each row must contain one of the possible
#'  combinations of character strings. The number of columns must be the same as
#'  the length of \code{ref}.
#'@return A vector with the same elements as \code{ref} but ordered as indicated
#'  by the entries in \code{lookup_tbl}.
#'@examples
#'lookup_tbl <- gtools::combinations(n = 3, r = 3, v = letters[1:3], rep = T)
#'lookup_entry(c("c", "a", "c"), lookup_tbl)
#'lookup_entry_list(list(c("c", "a", "c"), c("a", "c", "b")), lookup_tbl)
#'@export
lookup_entry <- function(ref, lookup_tbl) {
  stopifnot(ncol(lookup_tbl) == length(ref))
  r <- length(ref)

  perm  <- t(gtools::permutations(n = r, r = r, v = ref, set = F))

  out <- sapply(
    split(perm, col(perm)),
    function(x) max(colSums(x  == t(lookup_tbl))) == r
  )
  out <- perm[, out]
  if(is.matrix(out)) {
    out <- unique(split(out, col(out)))
    # check if all the same
    stopifnot(length(out) == 1)
    out <- unlist(out)
  }
  out
}
###############################################################################.
#'@rdname lookup_entry
#'@export
lookup_entry_list <- Vectorize(lookup_entry, vectorize.args = "ref",
                               SIMPLIFY = F)
